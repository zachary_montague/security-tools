import boto3
import time
import os
import logging
from botocore.exceptions import ClientError

# Setup Logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

COLORS = {
    'green': '\033[92m',
    'yellow': '\033[93m',
    'blue': '\033[94m',
    'red': '\033[91m',
    'white': '\033[0m',
}

MON_IPS = os.getenv('MON_IPS', '').split(',')

# Use AWS default configuration (e.g., from ~/.aws/credentials and ~/.aws/config)
ec2_client = boto3.client('ec2', region_name='eu-west-1')


def get_public_ip(ec2, instance_ids):
    try:
        response = ec2.describe_instances(InstanceIds=instance_ids)
        ips = [
            ni['Association']['PublicIp']
            for r in response['Reservations']
            for i in r['Instances']
            for ni in i['NetworkInterfaces']
        ]
        return ips
    except ClientError as e:
        logger.error(f"Error fetching public IP: {e}")
        return []


def stop_ec2(ec2, instance_ids):
    try:
        ec2.stop_instances(InstanceIds=instance_ids, Hibernate=False, Force=True)
        logger.info("Stopping Service")
        time.sleep(60)
        logger.info("Service Stopped")
    except ClientError as e:
        logger.error(f"Error stopping instances: {e}")


def start_ec2(ec2, instance_ids):
    try:
        ec2.start_instances(InstanceIds=instance_ids)
        logger.info("Starting Service")
        time.sleep(60)
        logger.info("Service Started")
    except ClientError as e:
        logger.error(f"Error starting instances: {e}")


def main():
    inst_ids = os.getenv('INST_IDS', '').split(',')
    if not inst_ids or not MON_IPS:
        logger.error("Missing required environment variables: INST_IDS or MON_IPS.")
        return

    found = False
    while not found:
        start_ec2(ec2_client, inst_ids)
        public_ip = get_public_ip(ec2_client, inst_ids)[0]
        logger.info(f"Current Instance IP is {public_ip}")
        for target_ip in MON_IPS:
            if public_ip == target_ip:
                found = True
                logger.info(f"Matched: {target_ip} = {public_ip}")
                break
            else:
                logger.info(f"Not Matched: {target_ip} != {public_ip}")
        if not found:
            logger.info("-- Trying Again --")
            time.sleep(3)
            stop_ec2(ec2_client, inst_ids)


if __name__ == "__main__":
    main()
