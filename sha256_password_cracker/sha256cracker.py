from pwn import *
import sys
import re
import hashlib


def is_sha256_hash(input_str):
    return len(input_str) == 64 and re.fullmatch(r'[0-9a-fA-F]{64}', input_str)


def sha256sum(data):
    return hashlib.sha256(data).hexdigest()


def main(desired_hash):
    if not is_sha256_hash(desired_hash):
        print("The input is not a valid SHA-256 hash. Please input a 64-character hash only.")
        sys.exit(1)

    password_file = "rockyou.txt"
    attempts = 0

    with log.progress("Attempting to crack: {}\n".format(desired_hash)) as p:
        with open(password_file, "r", encoding='latin-1') as password_list:
            for password in password_list:
                password = password.strip("\n").encode('latin-1')
                password_hash = sha256sum(password)
                p.status("[{}] {} == {}".format(attempts, password.decode('latin-1', 'ignore'), password_hash))
                if password_hash == desired_hash:
                    p.success("Hash found after {} attempts. {} hashes to {}.".format(attempts, password.decode('latin-1', 'ignore'), password_hash))
                    sys.exit(0)
                attempts += 1


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Invalid arguments")
        print("Usage: {} <sha256sum>".format(sys.argv[0]))
        sys.exit(1)

    desired_hash = sys.argv[1]
    main(desired_hash)
