use std::env;
use bruteforce_login::bruteforce;
use bruteforce_login::utils::read_lines;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() < 5 {
        eprintln!("Usage: <target_url> <username1> <username2> ... <password_file> <success_indicator>");
        std::process::exit(1);
    }

    let target_url = &args[1];
    let usernames: Vec<String> = args[2..args.len() - 2].to_vec();
    let password_file = &args[args.len() - 2];
    let success_indicator = &args[args.len() - 1];

    bruteforce::brute_force_login(target_url, &usernames, password_file, success_indicator)?;

    Ok(())
}
