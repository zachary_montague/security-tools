import os
import subprocess
import platform
import sys
import requests
from requests.exceptions import SSLError
from urllib.parse import urlparse

class Log4ShellChecker:
    def __init__(self, urls_file, wordlist_path):
        self.urls_file = urls_file
        self.wordlist_path = wordlist_path

    def run(self):
        if not self.should_run():
            sys.exit(1)

        wordlist = self.read_wordlist(self.wordlist_path)
        if wordlist is None:
            print(f"Error: could not read wordlist from {self.wordlist_path}")
            sys.exit(1)

        urls = self.read_urls(self.urls_file)
        if urls is None:
            print(f"Error: could not read URLs from {self.urls_file}")
            sys.exit(1)

        validated_urls = self.validate_urls(urls)

        for base_url in validated_urls:
            directories = self.dirbuster(base_url, wordlist)

            for directory in directories:
                self.check_log4j_version(directory)

    @staticmethod
    def should_run():
        os_type = platform.system()
        if os_type not in ('Linux', 'Windows'):
            print("This script is designed to run on Linux or Windows systems.")
            return False

        java_installed = subprocess.getoutput("java -version")
        if 'java version' not in java_installed and 'openjdk version' not in java_installed:
            print("Java is not installed on this system.")
            return False

        return True

    @staticmethod
    def read_wordlist(path):
        try:
            with open(path, 'r') as file:
                return [line.strip() for line in file if line.strip()]
        except IOError as e:
            print(f"Error reading wordlist from {path}: {e}")
            return None

    @staticmethod
    def read_urls(path):
        try:
            with open(path, 'r') as file:
                return [line.strip() for line in file if line.strip()]
        except IOError as e:
            print(f"Error reading URLs from {path}: {e}")
            return None

    @staticmethod
    def validate_urls(urls):
        validated_urls = []
        for url in urls:
            parsed_url = urlparse(url)
            if not parsed_url.scheme:
                url = 'http://' + url
                parsed_url = urlparse(url)
            if parsed_url.netloc:
                validated_urls.append(url)
            else:
                print(f"Invalid URL: {url}")
        return validated_urls

    @staticmethod
    def dirbuster(base_url, wordlist):
        directories = []
        for word in wordlist:
            url = f"{base_url}/{word}"
            try:
                response = requests.get(url, verify=False)
                if response.status_code == 200:
                    print(f"Directory found: {url}")
                    directories.append(url)
            except SSLError as e:
                print(f"SSL Error connecting to {url}: {e}")
        return directories

    @staticmethod
    def check_log4j_version(directory):
        for root, dirs, files in os.walk(directory):
            for file in files:
                if "log4j-core" in file and file.endswith(".jar"):
                    version = subprocess.getoutput(f"unzip -p {os.path.join(root, file)} META-INF/MANIFEST.MF | grep Implementation-Version")
                    print(f"Found log4j-core in {os.path.join(root, file)}, version: {version}")

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python script.py <urls_file> <wordlist>")
        sys.exit(1)

    urls_file = sys.argv[1]
    wordlist_path = sys.argv[2]
    checker = Log4ShellChecker(urls_file, wordlist_path)
    checker.run()
