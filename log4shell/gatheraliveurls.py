import requests
from bs4 import BeautifulSoup
import sys

def get_urls_from_crtsh(target_domain):
    base_url = 'https://crt.sh/?q=%25.' + target_domain
    response = requests.get(base_url)
    if response.status_code != 200:
        print(f"Error: could not retrieve data from crt.sh for domain {target_domain}")
        return None
    
    soup = BeautifulSoup(response.content, 'html.parser')
    td_elements = soup.find_all('td', class_='outer')
    urls = set()
    for td in td_elements:
        anchors = td.find_all('a')
        for a in anchors:
            text = a.get_text(strip=True)
            if '.' in text:
                urls.add(text)
    
    return urls

def check_live_urls(urls):
    live_urls = []
    for url in urls:
        try:
            response = requests.get('http://' + url, timeout=5)
            if response.status_code == 200:
                live_urls.append(url)
        except requests.RequestException:
            continue
    
    return live_urls

def write_urls_to_file(urls, file_path):
    with open(file_path, 'w') as file:
        for url in live_urls:
            file.write(url + '\n')

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py <target_domain>")
        sys.exit(1)

    target_domain = sys.argv[1]
    urls = get_urls_from_crtsh(target_domain)
    if urls is not None:
        live_urls = check_live_urls(urls)
        file_path = target_domain + '_live_urls.txt'
        write_urls_to_file(live_urls, file_path)
        print(f"Live URLs written to {file_path}")
